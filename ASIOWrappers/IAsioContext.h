#pragma once
#include "stdio.h"
#include <cstdint>
#include <functional>
#include "asio.hpp"
using FunctionHandler = std::function<void()>;

class IAsioContext
{
public:
    using ptr = std::shared_ptr<IAsioContext>;


    IAsioContext() = default;
    virtual ~IAsioContext() = default;

    virtual void run() = 0;
    virtual void stop() = 0;
    virtual void post(FunctionHandler f) = 0;
    virtual asio::io_context &getContext() = 0;
};

class AsioContext : public IAsioContext
{
public:
    AsioContext() : IAsioContext(){};
    virtual ~AsioContext() = default;

    virtual void run() override
    {
        m_context.run();
    }

    virtual void stop() override
    {
        m_context.stop();
    }

    virtual void post(FunctionHandler f) override
    {
        m_context.post(std::move(f));
    }

    virtual asio::io_context &getContext() override
    {
        return m_context;
    }

private:
    asio::io_context m_context;
};