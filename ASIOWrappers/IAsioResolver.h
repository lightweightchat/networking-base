#pragma once
#include "asio.hpp"
#include "IAsioContext.h"
#include <string>
class IAsioResolver
{
public:
    using ptr = std::shared_ptr<IAsioResolver>;
    IAsioResolver() = default;
    virtual ~IAsioResolver() = default;
    virtual asio::ip::tcp::resolver::results_type getEndPoint(const std::string &address, int port)=0;
};

class AsioResolver:public IAsioResolver{
    asio::ip::tcp::resolver resolver;
    public:
    AsioResolver(IAsioContext::ptr context):
        resolver(context->getContext())
        {}
    asio::ip::tcp::resolver::results_type getEndPoint(const std::string &address, int port) override{
        return resolver.resolve(address,std::to_string(port));
    }
};
