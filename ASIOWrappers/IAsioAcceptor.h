#pragma once
#include "stdio.h"
#include <cstdint>
#include <functional>
#include "asio.hpp"
#include "IAsioSocket.h"

using AcceptCallback = std::function<void(const asio::error_code &ec)>;

enum class IP
{
    V4,
    V6
};

class IAsioAcceptor
{
public:
    using ptr = std::shared_ptr<IAsioAcceptor>;

    IAsioAcceptor() = default;
    virtual ~IAsioAcceptor() = default;

    virtual void asyncAccept(IAsioSocket::ptr socket, AcceptCallback callback) = 0;
    virtual void close()=0;
    virtual asio::ip::tcp::acceptor &getAcceptor() = 0;
};

class AsioAcceptor : public IAsioAcceptor
{
public:
    AsioAcceptor(IAsioContext::ptr context, IP ipVersion, int port) : IAsioAcceptor(), 
    m_acceptor(context->getContext(), asio::ip::tcp::endpoint(ipVersion == IP::V4 ? asio::ip::tcp::v4() : asio::ip::tcp::v6(), port))
    {

    }

    virtual ~AsioAcceptor() = default;

    virtual void asyncAccept(IAsioSocket::ptr socket, AcceptCallback callback) override
    {
        m_acceptor.async_accept(socket->getSocket(), callback);
    }
    virtual void close() override
    {
        m_acceptor.close();
    }

    virtual asio::ip::tcp::acceptor &getAcceptor() override
    {
        return m_acceptor;
    }

private:
    asio::ip::tcp::acceptor m_acceptor;
};