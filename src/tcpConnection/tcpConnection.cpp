#include "tcpConnection.h"
#include <bit>
#include <version>
#include <type_traits>

TCPConnection::TCPConnection(IAsioSocket::ptr socket_)
    : ITCPConnection()
    , socket(socket_)
{
    
}

void TCPConnection::Start(onMessageCallback messageHandler, onErrorCallback errorHandler)
{
    this->messageHandler = messageHandler;
    this->errorHandler = errorHandler;
    username = socket->getEndPoint();
    asyncRead();
}

void TCPConnection::Stop()
{
    socket->close();
}

void TCPConnection::readHeaderHandler(asio::error_code ec){
    if (ec)
    {
        internalErrorReaction(ec);
        return;
    }      

    if(isBigEndian()){
        rcvHeader.data.protoMsgSize=__builtin_bswap64(rcvHeader.data.protoMsgSize);
        rcvHeader.data.startFlag=__builtin_bswap64(rcvHeader.data.startFlag);
    }
    if(rcvHeader.data.startFlag != Header_t::StartFlag){
        socket->close();
        ec.assign(asio::error::operation_not_supported,asio::error::misc_category);
        errorHandler(ec);
        return;
    }
    if(rcvHeader.data.protoMsgSize>0){
        readBody(rcvHeader.data.protoMsgSize);
    }
    else{
        readHeader();
    } 
}
void TCPConnection::readBodyHandler(asio::error_code ec, size_t bytesTranferred){
    if(ec){
        internalErrorReaction(ec);
        return;
    }
    messageHandler(BodyHeader.data(),bytesTranferred);
    readHeader();
}
void TCPConnection::readHeader(){
    socket->read(reinterpret_cast<uint8_t*>(&rcvHeader),sizeof(rcvHeader),[self = shared_from_this()](asio::error_code ec, size_t bytesTransferred) {
        self->readHeaderHandler(ec);
    });
}
void TCPConnection::readBody(size_t bodySize){
    BodyHeader.resize(bodySize);
    socket->read(BodyHeader.data(),bodySize,[self = shared_from_this()](asio::error_code ec, size_t bytesTransferred) {
        self->readBodyHandler(ec,bytesTransferred);
    });
}

void TCPConnection::asyncRead()
{
    readHeader();
}

void TCPConnection::asyncWrite()
{
    auto message = outgoingMessages.front();

    socket->write(message.data(), message.size(), [self = shared_from_this()](asio::error_code ec, size_t bytesTransferred) {
        self->onWrite(ec, bytesTransferred);
    });
}

void TCPConnection::onWrite(asio::error_code ec, size_t bytesTransferred)
{
    if (ec)
    {
        internalErrorReaction(ec);
        return;
    }
    outgoingMessages.pop();

    if (!outgoingMessages.empty())
    {
        asyncWrite();
    }
}

void TCPConnection::internalErrorReaction(asio::error_code ec){
    if(ec!=asio::error::operation_aborted){ //error means that connection is already closed
        socket->close();
        errorHandler(ec);
    }
}

void TCPConnection::Post(const uint8_t* message, size_t msgSize)
{
    bool queueIdle = outgoingMessages.empty();
    Header_t txHeader(msgSize);

    if(isBigEndian()){
        txHeader.data.protoMsgSize=__builtin_bswap64(txHeader.data.protoMsgSize);
        txHeader.data.startFlag=__builtin_bswap64(txHeader.data.startFlag);
    }

    auto ptr = reinterpret_cast<uint8_t*>(&(txHeader.data));
    auto buffer = std::vector<uint8_t>(&ptr[0], ptr+sizeof(txHeader.data));
    buffer.insert(buffer.end(),message,message+msgSize);
    buffer.shrink_to_fit();
    outgoingMessages.push(buffer);
    if (queueIdle)
    {
        asyncWrite();
    }
}

std::string TCPConnection::getUsername() const
{
    return username;
}
