#pragma once
#include <string>

struct ClientInfo
{

    ClientInfo(std::string username = "Unnamed", std::string ip = "0.0.0.0")
        : m_username(username), m_ip(ip)
    {
    }

    std::string m_ip;
    std::string m_username;
};

static bool operator==(const ClientInfo &lhs, const ClientInfo &rhs)
{
    return (lhs.m_ip == rhs.m_ip) || (lhs.m_username == rhs.m_username);
}