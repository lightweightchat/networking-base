#include "HeaderResolver.h"

void IResolver::setDirectHeaderCallback(DirectHeaderCallback_t callback)
{
    dmCallback = callback;
}
void IResolver::setBroadcastHeaderCallback(BroadcastHeaderCallback_t callback)
{
    broadcastCallback = callback;
}
void IResolver::setLoginHeaderCallback(LoginHeaderCallback_t callback)
{
    loginCallback = callback;
}
void IResolver::setUserConnectionStateChangedCallback(UserConnectionStateChangedCallback_t callback)
{
    uConnStateChangedCallback = callback;
}

void IResolver::setConnectedUsersCallback(ConnectedUsersHeaderCallback_t callback)
{
    connectedUsersCallback = callback;
}

void IResolver::setServerLogCallback(ConnectedUsersHeaderCallback_t callback)
{
    serverLogCallback = callback;
}

void IResolver::setKeepAliveHeaderCallback(KeepAliveHeaderCallback_t callback)
{
    kaCallback = callback;
}
void IResolver::setUndefinedHeaderCallback(UndefinedHeaderCallback_t callback)
{
    umCallback = callback;
}

void HeaderResolver::parseHeader(std::vector<uint8_t> &controlHeader, std::vector<uint8_t> &message, ITCPConnection::ptr connection)
{
    google::protobuf::Any any;
    any.ParseFromArray(controlHeader.data(), controlHeader.size());
    if (any.Is<NetworkingMessages::BroadcastHeader>())
    {
        if (broadcastCallback)
        {
            NetworkingMessages::BroadcastHeader bm;
            any.UnpackTo(&bm);
            broadcastCallback(bm.sender(), message, connection);
        }
    } else if (any.Is<NetworkingMessages::DirectHeader>())
    {
        if (dmCallback)
        {
            NetworkingMessages::DirectHeader dm;
            any.UnpackTo(&dm);
            dmCallback(dm.sender(), dm.receiver(), message, connection);
        }
    } else if (any.Is<NetworkingMessages::LoginHeader>())
    {
        if (loginCallback)
        {
            NetworkingMessages::LoginHeader lm;
            any.UnpackTo(&lm);
            loginCallback(lm.username(), lm.oldusername(), connection);
        }
    } else if (any.Is<NetworkingMessages::KeepAliveHeader>())
    {
        if (kaCallback)
        {
            kaCallback(connection);
        }
    } else if (any.Is<NetworkingMessages::UserConnectionStateChangedHeader>())
    {
        NetworkingMessages::UserConnectionStateChangedHeader uDisconnectHeader;
        any.UnpackTo(&uDisconnectHeader);
        if (uConnStateChangedCallback)
        {
            uConnStateChangedCallback(uDisconnectHeader.username(), uDisconnectHeader.isconnected(), connection);
        }
    } else if (any.Is<NetworkingMessages::ConnectedUsersHeader>())
    {
        NetworkingMessages::ConnectedUsersHeader connectedUsersHeader;
        any.UnpackTo(&connectedUsersHeader);
        
        if (connectedUsersCallback)
        {
            connectedUsersCallback(connectedUsersHeader.userlist(), connection);
        }
    } else if (any.Is<NetworkingMessages::ServerLogHeader>())
    {
        NetworkingMessages::ServerLogHeader serverLogHeader;
        any.UnpackTo(&serverLogHeader);
        if (serverLogCallback)
        {
           serverLogCallback(serverLogHeader.logs(), connection);
        }
    } else
    {
        if (umCallback)
            umCallback(connection);
    }
}
