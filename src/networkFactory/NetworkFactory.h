#pragma once
#include "IAsioAcceptor.h"
#include "IAsioContext.h"
#include "IAsioSocket.h"
#include "tcpConnection.h"
#include "IAsioResolver.h"
#include <memory>

class INetworkFactory
{
public:
    INetworkFactory() = default;
    virtual ~INetworkFactory() = default;

public:
    virtual IAsioContext::ptr makeContext() = 0;
    virtual IAsioSocket::ptr makeSocket(IAsioContext::ptr context) = 0;
    virtual IAsioAcceptor::ptr makeAcceptor(IAsioContext::ptr context, IP version, int port) = 0;
    virtual ITCPConnection::ptr makeTCPConnection(IAsioSocket::ptr socket) = 0;
    virtual IAsioResolver::ptr makeIpResolver(IAsioContext::ptr context) = 0;
};

class NetworkFactory : public INetworkFactory
{
public:
    NetworkFactory() = default;
    virtual ~NetworkFactory() = default;

public:
    virtual IAsioContext::ptr makeContext() override;
    virtual IAsioSocket::ptr makeSocket(IAsioContext::ptr context) override;
    virtual IAsioAcceptor::ptr makeAcceptor(IAsioContext::ptr context, IP version, int port) override;
    virtual ITCPConnection::ptr makeTCPConnection(IAsioSocket::ptr socket) override;
    virtual IAsioResolver::ptr makeIpResolver(IAsioContext::ptr context) override;
};