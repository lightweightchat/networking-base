#include "HeaderSerializer.h"

bool HeaderSerializer::makeDirectHeader(std::vector<uint8_t>& serializedMsgOut, const std::string& sender,const std::string& receiver){
    NetworkingMessages::DirectHeader dh;
    dh.set_sender(sender);
    dh.set_receiver(receiver);
    return serialize(serializedMsgOut,std::move(dh));
}
bool HeaderSerializer::makeBroadcastHeader(std::vector<uint8_t>& serializedMsgOut, const std::string& sender){
    NetworkingMessages::BroadcastHeader bh;
    bh.set_sender(sender);
    return serialize(serializedMsgOut,std::move(bh));
}
bool HeaderSerializer::makeLoginHeader(std::vector<uint8_t>& serializedMsgOut, const std::string& login, const std::string& oldLogin){
    NetworkingMessages::LoginHeader lh;
    lh.set_username(login);
    lh.set_oldusername(oldLogin);
    return serialize(serializedMsgOut,std::move(lh));
}
bool HeaderSerializer::makeKeepAliveHeader(std::vector<uint8_t>& serializedMsgOut){
    NetworkingMessages::KeepAliveHeader ka;
    return serialize(serializedMsgOut,std::move(ka));
}
bool HeaderSerializer::makeUserConnectionStateChangedHeader(std::vector<uint8_t>& serializedMsgOut, const std::string& username,bool isConnected){
    NetworkingMessages::UserConnectionStateChangedHeader uDisconnectHeader;
    uDisconnectHeader.set_username(username);
    uDisconnectHeader.set_isconnected(isConnected);
    return serialize(serializedMsgOut,std::move(uDisconnectHeader));
}

bool HeaderSerializer::makeConnectedUsersHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &userList)
{
    NetworkingMessages::ConnectedUsersHeader connectedUserHeader;
    connectedUserHeader.set_userlist(userList);
    return serialize(serializedMsgOut,std::move(connectedUserHeader));
}

bool HeaderSerializer::makeServerLogHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &logs)
{
    NetworkingMessages::ServerLogHeader serverLogHeader;
    serverLogHeader.set_logs(logs);
    return serialize(serializedMsgOut,std::move(serverLogHeader));
}
