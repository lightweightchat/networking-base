#pragma once
#include "Messages.pb.h"
#include <string>
#include <vector>

class ISerializer
{
public:
    using ptr = std::shared_ptr<ISerializer>;
    virtual bool makeDirectHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &sender, const std::string &receiver) = 0;
    virtual bool makeBroadcastHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &sender) = 0;
    virtual bool makeLoginHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &login, const std::string &oldLogin) = 0;
    virtual bool makeKeepAliveHeader(std::vector<uint8_t> &serializedMsgOut) = 0;
    virtual bool makeUserConnectionStateChangedHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &username, bool isConnected) = 0;
    virtual bool makeConnectedUsersHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &userList) = 0;
    virtual bool makeServerLogHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &logs) = 0;

    virtual ~ISerializer() = default;
};

class HeaderSerializer : public ISerializer
{
    template <typename T>
    bool serialize(std::vector<uint8_t> &serializedMsgOut, T &&message)
    {
        google::protobuf::Any any;
        any.PackFrom(message);
        serializedMsgOut.resize(any.ByteSizeLong());
        auto retVal = any.SerializeToArray(&serializedMsgOut[0], any.ByteSizeLong());
        serializedMsgOut.shrink_to_fit();
        return retVal;
    }

public:
    virtual bool makeDirectHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &sender, const std::string &receiver) override;
    virtual bool makeBroadcastHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &sender) override;
    virtual bool makeLoginHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &login, const std::string &oldLogin) override;
    virtual bool makeKeepAliveHeader(std::vector<uint8_t> &serializedMsgOut) override;
    virtual bool makeUserConnectionStateChangedHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &username, bool isConnected) override;
    virtual bool makeConnectedUsersHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &userList) override;
    virtual bool makeServerLogHeader(std::vector<uint8_t> &serializedMsgOut, const std::string &logs) override;
};