
#include "Encryptor.h"

Encryptor::Encryptor(std::shared_ptr<AES> aes, std::vector<unsigned char> key)
    : m_aes(std::move(aes))
{
    if (key.empty() || key.size() != encryptorDataLengthBlock)
    {
        m_key = defaultKey;
    } else
    {
        m_key = key;
    }
}

bool Encryptor::encrypt(std::vector<unsigned char> &data)
{
    if (!isDataAligned(data))
    {
        addPadding(data);
    }

    data = m_aes->EncryptECB(data, m_key);

    return true;
}

bool Encryptor::decrypt(std::vector<unsigned char> &data)
{
    data = m_aes->DecryptECB(data, m_key);
    return true;
}

void Encryptor::addPadding(std::vector<unsigned char> &data)
{
    if (!isDataAligned(data))
    {
        if (data.size() > encryptorDataLengthBlock)
        {
            data.resize(data.size() + (encryptorDataLengthBlock - (data.size() % encryptorDataLengthBlock)));
        } else
        {
            data.resize(data.size() + (encryptorDataLengthBlock - data.size()));
        }
    }
}

bool Encryptor::isDataAligned(std::vector<unsigned char> &data) const
{
    return ((data.size() % encryptorDataLengthBlock) == 0);
}
