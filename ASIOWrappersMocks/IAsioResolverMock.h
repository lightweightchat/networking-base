#pragma once

#include "IAsioResolver.h"
#include "gmock/gmock.h"

class AsioResolverMock : public IAsioResolver
{
public:
    MOCK_METHOD(asio::ip::tcp::resolver::results_type, getEndPoint, (const std::string &address, int port), (override));
    MOCK_METHOD0(Die, void());
    virtual ~AsioResolverMock() { Die(); }
};
