#pragma once

#include "IAsioAcceptor.h"
#include "gmock/gmock.h"

class AsioAcceptorMock : public IAsioAcceptor
{
public:
    MOCK_METHOD(void, asyncAccept, (IAsioSocket::ptr socket, AcceptCallback callback), (override));
    MOCK_METHOD(asio::ip::tcp::acceptor&, getAcceptor, (), (override));
    MOCK_METHOD(void, close, (), (override));

    MOCK_METHOD0(Die, void());
    virtual ~AsioAcceptorMock() { Die(); }
};
