#pragma once

#include "HeaderResolver.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class HeaderResolverMock : public IResolver
{
public:

  MOCK_METHOD(void, setDirectHeaderCallback, (DirectHeaderCallback_t callback), (override));
  MOCK_METHOD(void, setBroadcastHeaderCallback, (BroadcastHeaderCallback_t callback), (override));
  MOCK_METHOD(void, setLoginHeaderCallback, (LoginHeaderCallback_t callback), (override));
  MOCK_METHOD(void, setKeepAliveHeaderCallback, (KeepAliveHeaderCallback_t callback), (override));
  MOCK_METHOD(void, setUndefinedHeaderCallback, (UndefinedHeaderCallback_t callback), (override));
  MOCK_METHOD(void, setConnectedUsersCallback, (ConnectedUsersHeaderCallback_t callback), (override));
  MOCK_METHOD(void, setServerLogCallback, (ServerLogHeaderCallback_t callback), (override));
  MOCK_METHOD(void, parseHeader, (std::vector<uint8_t> &controlHeader, std::vector<uint8_t> &message, ITCPConnection::ptr connection), (override));

  MOCK_METHOD0(Die, void());
  virtual ~HeaderResolverMock() { Die(); }
};


