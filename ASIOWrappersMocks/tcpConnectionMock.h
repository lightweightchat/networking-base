#pragma once

#include "tcpConnection.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class TCPConnectionMock : public ITCPConnection
{
public:
    MOCK_METHOD(void, Start, (onMessageCallback messageHandler, onErrorCallback errorHandler), (override));
    MOCK_METHOD(void, Stop, (), (override));
    MOCK_METHOD(void, Post, (const uint8_t* message,size_t msgSize), (override));
    MOCK_METHOD0(Die, void());
    virtual ~TCPConnectionMock() { Die(); }
};
