#pragma once

#include "IAsioSocket.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

class AsioSocketMock : public IAsioSocket
{
public:
    MOCK_METHOD(void, close, (), (override));
    MOCK_METHOD(void, connect, (const asio::ip::tcp::endpoint &endpoint, ConnectCallback callback), (override));
    MOCK_METHOD(void, connect, (const asio::ip::tcp::resolver::results_type &endpoint, ConnectCallback callback), (override));

    MOCK_METHOD(void, read, (uint8_t * buffer, size_t maxLength, ReadCallback callback), (override));
    MOCK_METHOD(void, readUntil, (asio::streambuf & buffer, std::string delimiter, ReadCallback callback), (override));

    MOCK_METHOD(void, write, (const uint8_t *buffer, size_t maxLength, WriteCallback callback), (override));

    MOCK_METHOD(std::string, getEndPoint, (), (override));
    MOCK_METHOD(asio::ip::tcp::socket &, getSocket, (), (override));

    MOCK_METHOD0(Die, void());
    virtual ~AsioSocketMock() { Die(); }
    AsioSocketMock(){};
};
