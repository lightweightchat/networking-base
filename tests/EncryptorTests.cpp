#include "Encryptor.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <memory>
#include <vector>

using namespace ::testing;

class EncryptorTest : public Test
{
    void SetUp()
    {
        aes = std::make_shared<AES>(AESKeyLength::AES_128);
        encryptor = std::make_shared<Encryptor>(aes);
    }

public:
    std::shared_ptr<AES> aes;
    std::shared_ptr<Encryptor> encryptor;

};

TEST_F(EncryptorTest, Encryptor_Should_Add_Padding_To_Data_When_Data_Size_Is_Less_Than_AES_Size)
{
    std::vector<unsigned char> plainText = { 0x00, 0x11};

    encryptor->encrypt(plainText);

    EXPECT_TRUE((plainText.size()%16) == 0);
}

TEST_F(EncryptorTest, Encryptor_Should_Add_Padding_To_Data_When_Data_Size_Is_Greater_Than_AES_Size)
{
    std::vector<unsigned char> plainText = { 0x00, 0x11, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

    encryptor->encrypt(plainText);

    EXPECT_TRUE((plainText.size()%16) == 0);
}

TEST_F(EncryptorTest, Encryptor_Should_Not_Add_Padding_To_Data_When_Data_Size_Is_Equal_AES_Size)
{
    std::vector<unsigned char> plainText = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

    encryptor->encrypt(plainText);

    EXPECT_TRUE((plainText.size()%16) == 0);
}

TEST_F(EncryptorTest, Encryptor_Should_Encrypt_Data_Using_AES_Algorithm)
{
    std::vector<unsigned char> plainText = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    std::vector<unsigned char> expectedCipher = {0x27, 0x9f, 0xb7, 0x4a, 0x75, 0x72, 0x13, 0x5e, 0x8f, 0x9b, 0x8e, 0xf6, 0xd1, 0xee, 0xe0, 0x03}; 

    encryptor->encrypt(plainText);

    EXPECT_TRUE(plainText == expectedCipher);
}


TEST_F(EncryptorTest, Encryptor_Should_Decrypt_Data_Using_AES_Algorithm)
{
    std::vector<unsigned char> Cipher = {0x27, 0x9f, 0xb7, 0x4a, 0x75, 0x72, 0x13, 0x5e, 0x8f, 0x9b, 0x8e, 0xf6, 0xd1, 0xee, 0xe0, 0x03}; 
    std::vector<unsigned char> expectedplainText = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

    encryptor->decrypt(Cipher);

    EXPECT_TRUE(Cipher == expectedplainText);
}
  

   

// key 00112233445566778899aabbccddeeff
// text 000102030405060708090a0b0c0d0e0f