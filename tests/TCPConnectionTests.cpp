#include "IAsioSocketMock.h"
#include "tcpConnection.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <memory>
#include <cstring>
using namespace ::testing;

class TCPConnectionF : public Test
{
protected:
    std::vector<uint8_t> receivedData;
    bool errorCalled;
    std::shared_ptr<NiceMock<AsioSocketMock>> SocketMock;
    TCPConnection::ConnectionPointer connection;
    void SetUp()
    {
        SocketMock = std::make_shared<NiceMock<AsioSocketMock>>();
        receivedData.resize(0);
        errorCalled = false;
        connection = std::make_shared<TCPConnection>(SocketMock);
    }

public:
    void messageHandler(uint8_t* data, size_t dataLen)
    {
        receivedData.insert(receivedData.end(),data,data+dataLen);
    }
    void errHandler()
    {
        errorCalled = true;
    }
};

TEST_F(TCPConnectionF, StartShouldStartRead)
{
    EXPECT_CALL(*SocketMock, read(_, _, _)).Times(1);
    connection->Start(
        std::bind(&TCPConnectionF::messageHandler, this, std::placeholders::_1,std::placeholders::_2),
        std::bind(&TCPConnectionF::errHandler, this));
}

TEST_F(TCPConnectionF, StartShouldSetUsername)
{
    EXPECT_CALL(*SocketMock, getEndPoint()).Times(1).WillOnce(Return("NewName"));//?
    connection = std::make_shared<TCPConnection>(SocketMock);

     connection->Start(
        std::bind(&TCPConnectionF::messageHandler, this, std::placeholders::_1,std::placeholders::_2),
        std::bind(&TCPConnectionF::errHandler, this));
}

TEST_F(TCPConnectionF, CallbackOnReadShouldConsumeMessageAndSendToExternCallback)
{
    std::vector<uint8_t> dataReceivedFromTest={0,1,2,3,4,5,6,7,8,9};
    Expectation readHeader = EXPECT_CALL(*SocketMock, read(_, sizeof(_connection::Header_t), _)).Times(1). // if no error then start asyncRead again, so 2 times
        WillOnce(Invoke([](uint8_t * buffer, size_t maxLength, ReadCallback callback)
        {
            ASSERT_EQ(sizeof(_connection::Header_t), maxLength);
            _connection::Header_t headHeader(10);
            std::error_code ec(0, std::generic_category());
            memcpy(buffer,(uint8_t*)&headHeader,maxLength);
            callback(ec, sizeof(_connection::Header_t));
        }));
    Expectation readBody = EXPECT_CALL(*SocketMock, read(_, 10, _)).After(readHeader). // if no error then start asyncRead again, so 2 times
        WillOnce(Invoke([dataReceivedFromTest](uint8_t * buffer, size_t maxLength, ReadCallback callback)
        {
            ASSERT_EQ(10, maxLength);
            std::error_code ec(0, std::generic_category());
            memcpy(buffer,dataReceivedFromTest.data(),10);
            callback(ec, 10);
        }));
    Expectation HangOnReadingNextHeader = EXPECT_CALL(*SocketMock, read(_,sizeof(_connection::Header_t), _)).After(readBody);
    connection->Start(
        std::bind(&TCPConnectionF::messageHandler, this, std::placeholders::_1,std::placeholders::_2),
        std::bind(&TCPConnectionF::errHandler, this));
    ASSERT_TRUE(receivedData==dataReceivedFromTest);
}

TEST_F(TCPConnectionF, CallbackOnReadShouldCloseSocketOnError)
{
    EXPECT_CALL(*SocketMock, close()).Times(1);
    EXPECT_CALL(*SocketMock, read(_, _, _)).Times(1).WillOnce(Invoke([](uint8_t * buffer, size_t maxLength, ReadCallback callback)
        {
            std::error_code ec(10, std::generic_category());
            callback(ec, 10);
        }));
    connection->Start(
        std::bind(&TCPConnectionF::messageHandler, this, std::placeholders::_1,std::placeholders::_2),
        std::bind(&TCPConnectionF::errHandler, this));
    EXPECT_TRUE(errorCalled);
}

TEST_F(TCPConnectionF, PostShouldInvokeAsyncWriteOnEmptyQueue)
{
    uint8_t sentMsg[]={0,1,2,3,5,5,6,7,8,9};
    _connection::Header_t headHeader(sizeof(sentMsg));
    EXPECT_CALL(*SocketMock, write(_,_,_)).Times(1).WillOnce(Invoke([=](const uint8_t *buffer, size_t maxLength, WriteCallback callback){
        ASSERT_TRUE(std::memcmp(buffer,(uint8_t*)&headHeader.data,sizeof(_connection::Header_t::HeaderData))==0);//maxLength-sizeof(TCPConnection::Header_t::HeaderData)));
        ASSERT_TRUE(std::memcmp(buffer+16,sentMsg,10)==0);//maxLength-sizeof(TCPConnection::Header_t::HeaderData)));
    }));
    connection->Post(sentMsg,sizeof(sentMsg));
}

TEST_F(TCPConnectionF, CallbackOnWriteShouldStartWritteAgainIfQueueIsNotEmpty)
{
    WriteCallback callback_;
    EXPECT_CALL(*SocketMock, write(_, _, _)).Times(2).
        WillOnce(Invoke([&](const uint8_t* buffer, size_t bufferSize, WriteCallback callback)
                    {
                        callback_ = callback;
                    }))
            .WillOnce(Return());
    connection->Start(
    std::bind(&TCPConnectionF::messageHandler, this, std::placeholders::_1,std::placeholders::_2),
    std::bind(&TCPConnectionF::errHandler, this));
    uint8_t sentMsg[]={0,1,2,3,5,5,6,7,8,9};
    connection->Post(sentMsg,10);
    connection->Post(sentMsg,10);//queue two messages
    std::error_code ec(0, std::generic_category()); // no error
    callback_(ec, 10);  //simulate situation that one message is sent, onWrite callback is called.
                        //It calls write again because one message in queue left

}

TEST_F(TCPConnectionF, CallbackOnWriteShouldTurnCloseSocketOnError)
{
    EXPECT_CALL(*SocketMock, close()).Times(1);
    EXPECT_CALL(*SocketMock, write(_, _, _)).Times(1).
        WillOnce(Invoke([&](const uint8_t* buffer, size_t bufferSize, WriteCallback callback)
            {
                std::error_code ec(1, std::generic_category()); 
                callback(ec, 0);
            }));
    connection->Start(
        std::bind(&TCPConnectionF::messageHandler, this, std::placeholders::_1,std::placeholders::_2),
        std::bind(&TCPConnectionF::errHandler, this));
    uint8_t sentMsg[]={0,1,2,3,5,5,6,7,8,9};
    connection->Post(sentMsg,10);
    ASSERT_TRUE(errorCalled);
}
